package com.cybergrid.energyassets.aggregator.messaging;

import com.cybergrid.energyassets.aggregator.AggregatorService;
import com.cybergrid.energyassets.aggregator.EnergyDataMapper;
import com.cybergrid.energyassets.aggregator.model.EnergyData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class EnergyDataMessageListener {

    private static final Logger LOG = LoggerFactory.getLogger(EnergyDataMessageListener.class);

    private final AggregatorService aggregatorService;
    private final EnergyDataMapper dataMapper;

    public EnergyDataMessageListener(final AggregatorService aggregatorService, final EnergyDataMapper dataMapper) {
        this.aggregatorService = aggregatorService;
        this.dataMapper = dataMapper;
    }

    @RabbitListener(queues = "${energy-aggregator.messaging.queue}")
    public void receiveMessage(final EnergyData energyData) {
        LOG.info("RabbitMQ received message as specific class: {}", energyData.toString());
        aggregatorService.persistEnergyDataPoint(dataMapper.mapToEnergyMeasurement(energyData));
    }
}