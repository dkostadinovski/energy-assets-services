package com.cybergrid.energyassets.aggregator;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.cybergrid.energyassets.common"})
public class SpringComponentScanConfig {

}