package com.cybergrid.energyassets.aggregator;

import com.cybergrid.energyassets.common.redis.EnergyCacheService;
import com.cybergrid.energyassets.common.repository.EnergyDataRepository;
import com.cybergrid.energyassets.common.repository.EnergyMeasurement;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AggregatorService {

    private static final Logger LOG = Logger.getLogger(AggregatorService.class.getName());

    private final EnergyDataRepository repository;
    private final EnergyCacheService energyCacheService;

    public AggregatorService(final EnergyDataRepository repository, final EnergyCacheService energyCacheService) {
        this.repository = repository;
        this.energyCacheService = energyCacheService;
    }

    public void persistEnergyDataPoints(final List<EnergyMeasurement> energyMeasurementList) {
        repository.persistEnergyDataPoints(energyMeasurementList);
        // Cache data
        energyCacheService.putIntoCache(energyMeasurementList);
    }

    public void persistEnergyDataPoint(final EnergyMeasurement energyMeasurement) {
        repository.persistEnergyDataPoint(energyMeasurement);
        energyCacheService.putIntoCache(energyMeasurement);
    }

    public void updateEnergyDataPoint(long timePoint, EnergyMeasurement energyMeasurement) {
        repository.updateEnergyDataPoint(timePoint, energyMeasurement);
    }

    public void deleteEnergyDataPoints(long from, long to) {
        repository.deleteEnergyDataPoints(from, to);
    }
}
