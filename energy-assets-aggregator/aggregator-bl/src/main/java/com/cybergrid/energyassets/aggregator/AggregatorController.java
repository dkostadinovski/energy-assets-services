package com.cybergrid.energyassets.aggregator;

import com.cybergrid.energyassets.aggregator.api.EnergyDataApi;
import com.cybergrid.energyassets.aggregator.model.EnergyData;

import org.jboss.logging.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

@RestController
public class AggregatorController implements EnergyDataApi {

    private static final Logger LOG = Logger.getLogger(AggregatorController.class.getName());

    private final AggregatorService aggregatorService;

    private final EnergyDataMapper dataMapper;

    public AggregatorController(
        final AggregatorService aggregatorService, final EnergyDataMapper dataMapper) {
        this.aggregatorService = aggregatorService;
        this.dataMapper = dataMapper;
    }

    @Override
    public ResponseEntity<Void> deleteEnergyDataPoints(final Long from, final Long to) {
        return null;
    }

    @Override
    public ResponseEntity<Void> persistBulkEnergyData(@Valid final List<EnergyData> energyData) {
        try {
            aggregatorService.persistEnergyDataPoints(dataMapper.mapToEnergyMeasurementList(energyData));
            return ResponseEntity.created(new URI("/api/v1/energy-data")).build();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<Void> persistEnergyData(@Valid final EnergyData energyData) {
        try {
            aggregatorService.persistEnergyDataPoint(dataMapper.mapToEnergyMeasurement(energyData));
            return ResponseEntity.created(new URI("/api/v1/energy-data")).build();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<Void> updateEnergyDataPoint(final String timepoint, @Valid final EnergyData energyData) {
        aggregatorService.updateEnergyDataPoint(Long.parseLong(timepoint), dataMapper.mapToEnergyMeasurement(energyData));
        return ResponseEntity.noContent().build();
    }
}
