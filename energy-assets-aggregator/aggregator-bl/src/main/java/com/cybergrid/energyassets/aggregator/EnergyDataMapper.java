package com.cybergrid.energyassets.aggregator;

import com.cybergrid.energyassets.aggregator.model.EnergyData;
import com.cybergrid.energyassets.common.repository.EnergyMeasurement;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring",
    imports = {Instant.class})
public interface EnergyDataMapper {

    @Mapping(target = "time", expression = "java(Instant.ofEpochMilli(energyData.getTimepoint()))")
    EnergyMeasurement mapToEnergyMeasurement(EnergyData energyData);

    List<EnergyMeasurement> mapToEnergyMeasurementList(List<EnergyData> energyDataList);

    default String mapToString(final UUID uuid) {
        return uuid.toString();
    }

}
