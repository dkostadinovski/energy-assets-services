package com.cybergrid.energyassets.common.repository;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import org.influxdb.annotation.TimeColumn;

import java.io.Serializable;
import java.time.Instant;

import lombok.Data;

@Data
@Measurement(name = EnergyMeasurement.NAME)
public class EnergyMeasurement implements Serializable {

    public static final String NAME = "energy";

    public static final String FIELD_UNIT_ID = "unit_id";
    public static final String FIELD_ACTIVE_POWER = "active_power";
    public static final String FIELD_VOLTAGE = "voltage";
    public static final String FIELD_BASELINE = "baseline";
    public static final String FIELD_SET_POINT = "set_point";
    public static final String FIELD_POSITIVE_CAPACITY = "positive_capacity";
    public static final String FIELD_NEGATIVE_CAPACITY = "negative_capacity";

    @TimeColumn
    @Column(name = "time")
    private Instant time;

    @Column(name = FIELD_UNIT_ID)
    private String unitId;

    @Column(name = FIELD_ACTIVE_POWER)
    private Double activePower;

    @Column(name = FIELD_VOLTAGE)
    private Double voltage;

    @Column(name = FIELD_BASELINE)
    private Double baseline;

    @Column(name = FIELD_SET_POINT)
    private String setPoint;

    @Column(name = FIELD_POSITIVE_CAPACITY)
    private Double positiveCapacity;

    @Column(name = FIELD_NEGATIVE_CAPACITY)
    private Double negativeCapacity;
}
