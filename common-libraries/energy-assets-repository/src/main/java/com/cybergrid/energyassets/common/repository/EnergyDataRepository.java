package com.cybergrid.energyassets.common.repository;

import java.util.List;

/**
 * Repository for managing energy data into persistent storage.
 */
public interface EnergyDataRepository {

    /**
     * Persist data point into DB.
     *
     * @param energyMeasurement data to be persisted
     */
    void persistEnergyDataPoint(EnergyMeasurement energyMeasurement);

    /**
     * Persist data points into DB.
     *
     * @param energyData data points to be persisted
     */
    void persistEnergyDataPoints(List<EnergyMeasurement> energyData);

    /**
     * Update data point at given time point.
     *
     * @param timePoint time point of the data
     * @param energyMeasurement new data
     */
    void updateEnergyDataPoint(long timePoint, EnergyMeasurement energyMeasurement);

    /**
     * Delete data points in time range.
     *
     * @param from which time point
     * @param to which time point
     */
    void deleteEnergyDataPoints(long from, long to);

    /**
     * Retrieve energy data points.
     *
     * @param limit of the points returned
     * @param offset offset of the data
     * @return list of energy data points
     */
    List<EnergyMeasurement> getEnergyDataPoints(int limit, long offset);

    /**
     * Retrieve energy data points.
     *
     * @param unitId id of the unit
     * @param limit of the points returned
     * @param offset offset of the data
     * @return list of energy data points
     */
    List<EnergyMeasurement> getEnergyDataPoints(String unitId, int limit, long offset);
}
