package com.cybergrid.energyassets.common.influxdb;

import com.cybergrid.energyassets.common.repository.EnergyDataRepository;
import com.cybergrid.energyassets.common.repository.EnergyMeasurement;

import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.influxdb.InfluxDBTemplate;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static org.influxdb.querybuilder.BuiltQuery.QueryBuilder.eq;
import static org.influxdb.querybuilder.BuiltQuery.QueryBuilder.select;

@Repository
public class InfluxDbRepository implements EnergyDataRepository {

    private static final Logger LOG = Logger.getLogger(InfluxDbRepository.class.getName());

    @Value("${spring.influxdb.database}")
    private String database;

    private final InfluxDBTemplate<Point> influxDBTemplate;
    private final InfluxDBResultMapper resultMapper;

    public InfluxDbRepository(
        final InfluxDBTemplate<Point> influxDBTemplate) {
        this.influxDBTemplate = influxDBTemplate;

        this.resultMapper = new InfluxDBResultMapper();
    }

    @Override
    public void persistEnergyDataPoint(final EnergyMeasurement energyMeasurement) {
        final Point energyPoint = convertToEnergyPoint(energyMeasurement);
        influxDBTemplate.write(energyPoint);
    }

    @Override
    public void persistEnergyDataPoints(final List<EnergyMeasurement> energyData) {
        final List<Point> energyDataPoints = convertToEnergyPoints(energyData);
        influxDBTemplate.write(energyDataPoints);
        LOG.infov("{0} energy points successfully persisted.", energyDataPoints.size());
    }

    @Override
    public void updateEnergyDataPoint(final long timePoint, final EnergyMeasurement energyMeasurement) {
        energyMeasurement.setTime(Instant.ofEpochMilli(timePoint));
        final Point energyPoint = convertToEnergyPoint(energyMeasurement);
        influxDBTemplate.write(energyPoint);
    }

    @Override
    public void deleteEnergyDataPoints(final long from, final long to) {
        final Instant fromTime = Instant.ofEpochMilli(from);
        final Instant toTime = Instant.ofEpochMilli(to);
        final Query deleteQuery =
            new Query("DELETE FROM energy WHERE time > " + fromTime.toString() + " and time < " + toTime.toString());
        influxDBTemplate.query(deleteQuery);
    }

    @Override
    public List<EnergyMeasurement> getEnergyDataPoints(final int limit, final long offset) {
        final Query selectQuery = offset == 0 ?
            select().from(database, EnergyMeasurement.NAME).limit(limit) :
            select().from(database, EnergyMeasurement.NAME).limit(limit, offset);
        final QueryResult queryResult = influxDBTemplate.query(selectQuery);
        return resultMapper.toPOJO(queryResult, EnergyMeasurement.class);
    }

    @Override
    public List<EnergyMeasurement> getEnergyDataPoints(final String unitId, final int limit, final long offset) {
        final Query selectQuery = offset == 0 ?
            select()
                .from(database, EnergyMeasurement.NAME)
                .where(eq(EnergyMeasurement.FIELD_UNIT_ID, unitId))
                .limit(limit) :
            select()
                .from(database, EnergyMeasurement.NAME)
                .where(eq(EnergyMeasurement.FIELD_UNIT_ID, unitId))
                .limit(limit, offset);

        final QueryResult queryResult = influxDBTemplate.query(selectQuery);
        return resultMapper.toPOJO(queryResult, EnergyMeasurement.class);
    }

    private List<Point> convertToEnergyPoints(final List<EnergyMeasurement> energyData) {
        return energyData.stream()
            .map(this::convertToEnergyPoint)
            .collect(Collectors.toList());
    }

    private Point convertToEnergyPoint(final EnergyMeasurement energyMeasurement) {
        return Point
            .measurementByPOJO(EnergyMeasurement.class)
            .addFieldsFromPOJO(energyMeasurement)
            .build();
    }
}
