package com.cybergrid.energyassets.common.redis;

import com.cybergrid.energyassets.common.repository.EnergyMeasurement;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnergyCacheService {

    private final RedisTemplate<String, EnergyMeasurement> redisTemplate;

    public EnergyCacheService(final RedisTemplate<String, EnergyMeasurement> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void putIntoCache(final List<EnergyMeasurement> energyMeasurementList) {
        energyMeasurementList.forEach(this::putIntoCache);
    }

    public void putIntoCache(final EnergyMeasurement energyMeasurement) {
        redisTemplate.opsForList().leftPush(energyMeasurement.getUnitId(), energyMeasurement);
    }

    public List<EnergyMeasurement> retrieveFromCache(final String unitId) {
        return redisTemplate.opsForList().range(unitId, 0, -1);
    }
}
