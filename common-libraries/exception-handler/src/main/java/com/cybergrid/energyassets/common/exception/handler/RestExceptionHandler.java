package com.cybergrid.energyassets.common.exception.handler;

import org.jboss.logging.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = Logger.getLogger(RestExceptionHandler.class.getName());

    @ExceptionHandler(value = {Throwable.class})
    protected ResponseEntity<Object> handleGeneralException(Exception ex, WebRequest request) {
        final String exceptionUuid = UUID.randomUUID().toString();
        LOG.error("General exception occurred (" + exceptionUuid + ").", ex);

        return handleExceptionInternal(ex,
            new ErrorResponse("Internal Server Exception occurred. Reference: " + exceptionUuid),
            new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleBadRequestException(ConstraintViolationException ex, WebRequest request) {
        final String errorMessage = ex.getConstraintViolations().stream()
            .map(exception -> exception.getPropertyPath() + " " + exception.getMessage() + "; ")
            .collect(Collectors.joining());

        return handleExceptionInternal(ex,
            new ErrorResponse(errorMessage),
            new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
