package com.cybergrid.energyassets.aggregator.provider;

import com.cybergrid.energyassets.common.redis.EnergyCacheService;
import com.cybergrid.energyassets.common.repository.EnergyDataRepository;
import com.cybergrid.energyassets.common.repository.EnergyMeasurement;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderService {

    private final EnergyDataRepository repository;
    private final EnergyCacheService energyCacheService;

    public ProviderService(final EnergyDataRepository repository, final EnergyCacheService energyCacheService) {
        this.repository = repository;
        this.energyCacheService = energyCacheService;
    }

    public List<EnergyMeasurement> getEnergyData(int limit, long offset) {
        return this.repository.getEnergyDataPoints(limit, offset);
    }

    public List<EnergyMeasurement> getEnergyData(final String unitId, int limit, long offset) {
        final List<EnergyMeasurement> cachedEnergyData =  energyCacheService.retrieveFromCache(unitId);
        if(!cachedEnergyData.isEmpty()) {
            return cachedEnergyData;
        }
        return repository.getEnergyDataPoints(unitId, limit, offset);
    }
}
