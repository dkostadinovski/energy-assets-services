package com.cybergrid.energyassets.aggregator.provider;

import com.cybergrid.energyassets.provider.api.EnergyDataApi;
import com.cybergrid.energyassets.provider.model.EnergyData;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
public class ProviderController implements EnergyDataApi {

    private final ProviderService providerService;

    private final EnergyDataMapper mapper;

    public ProviderController(final ProviderService providerService,
        final EnergyDataMapper mapper) {
        this.providerService = providerService;
        this.mapper = mapper;
    }

    @Override
    public ResponseEntity<List<EnergyData>> getEnergyDataPoints(
        @Valid final Optional<UUID> unitId,
        @Valid final Optional<Long> from,
        @Valid final Optional<Long> to,
        @Valid final Optional<String> sort,
        @Min(1) @Max(1000) @Valid final Optional<Integer> limit,
        @Min(0L) @Valid final Optional<Long> offset) {

        final int pageLimit = limit.orElse(100);
        final long pageOffset = offset.orElse(0L);

        if (unitId.isPresent()) {
            return ResponseEntity.ok(
                mapper.mapToEnergyDataList(
                    providerService.getEnergyData(unitId.get().toString(), pageLimit, pageOffset)));
        }

        return ResponseEntity.ok(mapper.mapToEnergyDataList(providerService.getEnergyData(pageLimit, pageOffset)));
    }
}
