package com.cybergrid.energyassets.aggregator.provider;

import com.cybergrid.energyassets.common.repository.EnergyMeasurement;
import com.cybergrid.energyassets.provider.model.EnergyData;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface EnergyDataMapper {

    @Mapping(target = "timepoint", expression = "java(energyMeasurement.getTime().toEpochMilli())")
    EnergyData mapToEnergyData(EnergyMeasurement energyMeasurement);

    List<EnergyData> mapToEnergyDataList(List<EnergyMeasurement> energyMeasurementList);

    default UUID mapToUuid(final String stringUuid) {
        if (stringUuid != null && !stringUuid.isBlank()) {
            return UUID.fromString(stringUuid);
        }
        return null;
    }
}
