# Energy data managing services

## Description
Energy assets (PV, Wind, Battery) are scattered on the field. They are producing large amount of data
that should be send to the central database for the purpose of further analyse and real-time operation.


## Architecture
This is a simple microservices example based on Spring-boot features.

The design of the project assumes that GET operations have a key influence on the performance aspect, so focus was
 made to implement fully scalable and high performance GET operations. On the other hand POST, PUT and DELETE
  operations were encapsulated in another service which is also fully scalable.
  
Two services were implemented:
 * Aggregator Service (POST, PUT, DELETE requests)
 * Provider Service (GET requests)
 
Picture below illustrates the designed architecture!
![System Architecture](system-arhitecture.jpg)

### Repository structure

* **common-libraries**: common libraries used by both **aggregator** and **provider** services
* **energy-assets-aggregator**
    * **aggregator-api**: Includes only generated interface from OpenAPI documentation. It's useful when implementing
     REST client
    * **aggregator-bl**: Deployment unit, which encapsulates all business logic. Depends on **api**, **repositories** and **connectors**.
    * **aggregator-repositories**: Encapsulates DB level logic (in our case includes **influxdb-connector**)
    * **aggregator-connectors**: Encapsulates all connectors to external systems (TODO RabbitMQ connector should be
     moved here)
* **energy-assets-provider**
    * **provider-api**: Includes only generated interface from Swagger documentation. It's useful when implementing
     REST client
    * **aggregator-bl**: Deployment unit, which encapsulates all business logic. Depends on **api** and
     **repositories**.
    * **aggregator-repositories**: Encapsulates DB level logic (in our case includes **influxdb-connector**)
* **specs**: Contains OpenaAPI v3 documentation for both services
* **kubernetes**: Contains Kubernetes deployment file (*Note: this is only draft file and was never tested in real
 K8s environment)
* **docker**: Contains **docker-compose** file for running all platform services.

## Testing

To test this solution just run docker compose file in `docker` folder.

`docker-compose up`

For testing endpoints just import OpenAPI specifications (form `specs` folder) in `Postman`.

* **aggregator service** runs on port **8080**
* **provider service** runs on port **8090**

## Known issues
* When persisting data into InfluxDB, `energy` table is used. This is not the expected behaviour as
 different assets will override its data at the same time-point. This should be solved with using different table for
  each individual asset (eg. `energy-<unit_id>`).
  
## Features which were planned, but ran out-of-time
* load-balancer
* Health, metrics and fault-tolerance
* Better exception handling
* For better performance, smaller objects/JSONs payloads when sending messages through RabbitMQ and REST
* Integration tests (setting-up whole platform with `docker-compose` and running tests with `RestAssured`)